/* Generated for Instituto Vita */
'use strict';

module.exports = function(grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    var config = {
        dev    : 'dev',
        dist   : 'dist',
        static : 'static'
    };

    grunt.initConfig({

        config: config,

        watch: {
            options: {
                livereload: true
            },
            bower: {
                files: ['bower.json'],
                tasks: ['wiredep']
            },
            php: {
                files: './{,*/}*.php'
            },
            js: {
                files: ['<%= config.dev %>/src/js/{,*/}*.js'],
                // tasks: ['jshint'],
                options: {
                    livereload: true
                }
            },
            images: {
                files: '<%= config.dev %>/src/images/{,*/}*.{png,jpg,jpeg,gif,svg}',
                tasks: ['copy:images']
            },
            fonts: {
                files: '<%= config.dev %>/src/fonts/**/*',
                tasks: ['copy:fonts']
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            sass: {
                files: ['<%= config.dev %>/src/scss/{,*/}*.{scss,sass}'],
                tasks: ['sass:server', 'autoprefixer', 'notify:sassDone']
            },
            styles: {
                files: ['<%= config.static %>/css/{,*/}*.css'],
                // tasks: ['newer:copy:styles', 'autoprefixer']
            }
        },

        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    '<%= config.static %>/css/style.css': '<%= config.dev %>/src/scss/style.scss',
                    '<%= config.static %>/css/ie.css': '<%= config.dev %>/src/scss/ie.scss',
                }
            },
            server: {
                options: {
                    style: 'expanded'
                },
                files: {
                    '<%= config.static %>/css/style.css': '<%= config.dev %>/src/scss/style.scss',
                    '<%= config.static %>/css/ie.css': '<%= config.dev %>/src/scss/ie.scss',
                }
            }
        },

        autoprefixer: {
            options: {
                browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/styles/',
                    src: '{,*/}*.css',
                    dest: '.tmp/styles/'
                }]
            }
        },

        notify: {
            done: {
                options: {
                    title: 'Done!', // optional
                    message: 'Whatever you were doing is done!', //required
                }
            },
            distStart: {
                options: {
                    title: ' Prepping for distribution!', // optional
                    message: 'Get ready for the awesome...', //required
                }
            },
            distDone: {
                options: {
                    title: "All packaged up!", // optional
                    message: "Instituto Vita is ready to be distributed ", //needed escaping!
                }
            },
            sassDone: {
                options: {
                    title: ' Ta-da!!!', // optional
                    message: 'Sass has compiled successfully ', //required
                }
            },
            initStart: {
                options: {
                    title: 'Initializing project', // optional
                    message: '...', //required
                }
            },
            initDone: {
                options: {
                    title: 'Initialization done!', // optional
                    message: 'Run : "grunt watch" and get to work!', //required
                }
            },
        },

        clean: {
            server: {
                options: { force: true },
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= config.static %>/js/*',
                        '<%= config.static %>/css/*',
                        '<%= config.static %>/fonts/*',
                        '<%= config.static %>/images/*'
                    ]
                }]
            },
            dist: {
                options: { force: true },
                files: [{
                    dot: true,
                    src: [
                        'production/*',
                        '<%= config.dist %>/*',
                        '!<%= config.dist %>/.git*'
                    ]
                }]
            }
        },

        wiredep: {
            server: {
                ignorePath: /^\/|\.\.\//,
                src: ['header.php', 'footer.php'],
                exclude: ['bower_components/modernizr/modernizr.js'],
                options: {
                    // See wiredep's configuration documentation for the options you may pass:
                    // https://github.com/taptapship/wiredep#configuration
                }
            },
            sass: {
                src: ['<%= config.dev %>/src/scss/{,*/}*.{scss,sass}'],
                ignorePath: /(\.\.\/){1,2}bower_components\//
            }
        },

        modernizr: {
            server: {
                devFile: '<%= config.dev %>/bower_components/modernizr/modernizr.js',
                outputFile: '<%= config.static %>/js/modernizr.js',
                files: {
                    src: [
                        '<%= config.static %>/js/{,*/}*.js',
                        '<%= config.static %>/css/{,*/}*.css',
                        '!<%= config.static %>/js/vendor/*'
                    ]
                },
                uglify: true
            },
            dist: {
                devFile: '<%= config.dev %>/bower_components/modernizr/modernizr.js',
                outputFile: '<%= config.dist %>/static/js/modernizr.js',
                files: {
                    src: [
                        '<%= config.dist %>/static/js/{,*/}*.js',
                        '<%= config.dist %>/static/css/{,*/}*.css',
                        '!<%= config.dist %>/static/js/vendor/*'
                    ]
                },
                uglify: true
            }
        },

        replace: {
            server: {
                src: ['header.php', 'footer.php'],    // source files array (supports minimatch)
                dest: './',                           // destination directory or file
                replacements: [
                    {
                        from: 'dev/bower_components/',
                        to: '<?php echo get_template_directory_uri(); ?>/dev/bower_components/'
                    },
                    {
                        from: 'dev/src/',
                        to: '<?php echo get_template_directory_uri(); ?>/dev/src/'
                    },
                    {
                        from: 'styles/',
                        to: '<?php echo get_template_directory_uri(); ?>/static/css/'
                    }
                ]
            },
            prepare: {
                src: ['header.php', 'footer.php'],
                dest: './',
                replacements: [
                    {
                        from: '<?php echo get_template_directory_uri(); ?>/dev/src/',
                        to: 'dev/src/'
                    },
                    {
                        from: '<?php echo get_template_directory_uri(); ?>/static/css/',
                        to: 'styles/'
                    }
                ]
            },
            dist: {
                src: ['<%= config.dist %>/header.php', '<%= config.dist %>/footer.php'],
                dest: '<%= config.dist %>/',
                replacements: [
                    {
                        from: '"static/',
                        to: '"<?php echo get_template_directory_uri(); ?>/static/'
                    }
                ]
            }
        },

        useminPrepare: {
            options: {
                dest: '<%= config.dist %>'
            },
            html: '{,*/}*.php'
        },

        // Performs rewrites based on rev and the useminPrepare configuration
        usemin: {
            options: {
                assetsDirs: [
                    '<%= config.dist %>',
                    '<%= config.dist %>/static/images',
                    '<%= config.dist %>/static/css'
                ]
            },
            html: ['<%= config.dist %>/{,*/}*.php'],
            css: ['<%= config.dist %>/static/css/{,*/}*.css']
        },

        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.static %>/images',
                    src: '{,*/}*.{gif,jpeg,jpg,png}',
                    dest: '<%= config.dist %>/static/images'
                }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.static %>/images',
                    src: '{,*/}*.svg',
                    dest: '<%= config.dist %>/static/images'
                }]
            }
        },

        copy: {
            dev: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= config.dev %>/src/js',
                        src: ['*.js'],
                        dest: '<%= config.static %>/js',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        cwd: '<%= config.dev %>/src/images',
                        src: ['{,*/}*.{png,jpg,gif,svg}'],
                        dest: '<%= config.static %>/images',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        cwd: '<%= config.dev %>/src/fonts',
                        src: ['**/*'],
                        dest: '<%= config.static %>/fonts',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        cwd: '<%= config.dev %>/bower_components/bootstrap/dist/fonts',
                        dest: '<%= config.static %>/fonts',
                        src: '{,*/}*.*'
                    }
                ]
            },
            fonts: {
                expand: true,
                cwd: '<%= config.dev %>/src/fonts',
                src: ['**/*'],
                dest: '<%= config.static %>/fonts',
                filter: 'isFile'
            },
            images: {
                expand: true,
                cwd: '<%= config.dev %>/src/images',
                src: ['**/*.{png,jpg,jpeg,gif,svg}'],
                dest: '<%= config.static %>/images'
            },
            styles: {
                expand: true,
                dot: true,
                cwd: '<%= config.static %>/css',
                dest: '.tmp/styles/',
                src: '{,*/}*.css'
            },
            dist: {
                files: [{
                        expand: true,
                        dot: true,
                        cwd: '',
                        dest: '<%= config.dist %>',
                        src: [
                            '*.{ico,png,txt}',
                            '{,*/}*.php',
                            '{,*/}*.css',
                            '_inc/*'
                        ]
                    },
                    {
                        expand: true,
                        cwd: '<%= config.dev %>/src/fonts',
                        src: ['**/*'],
                        dest: '<%= config.dist %>/static/fonts',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        cwd: '<%= config.dev %>/bower_components/bootstrap/dist/fonts',
                        dest: '<%= config.dist %>/static/fonts',
                        src: '{,*/}*.*'
                    }
                ]
            }
        },

        // Run some tasks in parallel to speed up build process
        concurrent: {
            server: [
                'sass:server',
                'copy:styles'
            ],
            dist: [
                'sass',
                'copy:styles',
                'imagemin',
                'svgmin'
            ]
        },

        // make a zipfile
        compress: {
            production: {
                options: {
                    archive: 'production/institutovita.zip'
                },
                files: [{
                    src: ['<%= config.dist %>/**/*', '!<%= config.dev %>/**', '!production/**']
                }, ]
            }
        }

    }); //end grunt package configs


    //RUN FOR DEVELOPER
    grunt.registerTask('serve', ['notify:initStart', 'clean:server', 'copy:dev', 'wiredep', 'replace:server', 'concurrent:server', 'autoprefixer', 'modernizr:server', 'notify:initDone', 'watch']);

    //RUN FOR PRODUCTION
    grunt.registerTask('build', ['notify:distStart', 'clean:dist', 'wiredep', 'replace:prepare', 'useminPrepare', 'concurrent:dist', 'autoprefixer', 'concat', 'cssmin', 'uglify', 'modernizr:dist', 'copy:dist', 'usemin', 'replace:dist', 'compress:production', 'notify:distDone']);

    //DEFAULT
    grunt.registerTask('default', []);
};