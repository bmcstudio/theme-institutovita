<?php
/**
 * @package WordPress
 * @subpackage Instituto Vita
 * @since Instituto Vita 1.0
 */
 get_header(); ?>

 	<section class="bg-banner">
        <div class="container relative" style="height: 400px;">
            <div class="slogan">
                <p>Bem vindo ao novo</p>
                <p>Portal do <strong>Instituto Vita</strong></p>
            </div>
        </div>
    </section>

    <section class="bg-videos">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h1 class="title">Vídeos</h1>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/_U7QJaTHMJ4"></iframe>
                    </div>
                    <h3>Histórias de Vita - Leandro Guilheiro</h3>
                </div>
                <div class="col-md-5">
                    <h1 class="title">Destaques</h1>
                    <ul class="list-features">
                        <li>
                            <a href="" title="">
                            <div class="thumb-feature">
                                <img src="<?php echo get_template_directory_uri(); ?>/static/images/ex-video.jpg" class="img-responsive" alt="">
                            </div>
                            </a>
                            <div class="title-feature">
                                <a href="" title=""><h3>Acesse nosso blog</h3></a>
                            </div>
                        </li>
                        <li>
                            <a href="" title="">
                            <div class="thumb-feature">
                                <img src="<?php echo get_template_directory_uri(); ?>/static/images/ex-video.jpg" class="img-responsive" alt="">
                            </div>
                            </a>
                            <div class="title-feature">
                                <a href="" title=""><h3>Saiba mais sobre as especialidades do Vita</h3></a>
                            </div>
                        </li>
                        <li>
                            <a href="" title="">
                            <div class="thumb-feature">
                                <img src="<?php echo get_template_directory_uri(); ?>/static/images/ex-video.jpg" class="img-responsive" alt="">
                            </div>
                            </a>
                            <div class="title-feature">
                                <a href="" title=""><h3>Participe dos nossos cursos</h3></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-4 sm-left">
                    <div>
                        <h2 class="subtitle">Conheça o Vita</h2>
                        <a href="" title="">
                            <div class="img-feature relative pull-left">
                                <img src="<?php echo get_template_directory_uri(); ?>/static/images/image1.jpg" class="img-responsive" alt="">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 sm-left divider-left">
                    <h2 class="subtitle">Unidades</h2>
                    <a href="" title="">
                        <div class="img-feature relative pull-left">
                            <img src="<?php echo get_template_directory_uri(); ?>/static/images/image2.jpg" class="img-responsive" alt="">
                        </div>
                    </a>
                </div>
                <div class="col-md-4 sm-left divider-left">
                    <h2 class="subtitle">Teste de vitalidade</h2>
                    <a href="" title="">
                        <div class="img-feature relative pull-left">
                            <img src="<?php echo get_template_directory_uri(); ?>/static/images/image3.jpg" class="img-responsive" alt="">
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="subtitle">Nossos profissionais</h2>
                    <img src="<?php echo get_template_directory_uri(); ?>/static/images/image4.jpg" class="img-responsive" alt="" style="margin-bottom: 20px;">
                    <p>A tarefa mais significativa de toda equipe é unir a qualidade do atendimento à constante preocupação com a interação pessoal, buscando estabelecer um efetivo compromisso com o ser humano. Estamos com você nas mais diversas especialidades, como odontologia, nutrição, fisiologia, fisioterapia e medicina.</p>
                </div>
                <div class="col-md-6 divider-left">
                    <h2 class="subtitle">Notícias</h2>
                    <ul class="list-news">
                        <li>
                            <a href="" title="">
                            <div class="thumb-feature">
                                <img src="<?php echo get_template_directory_uri(); ?>/static/images/ex-video.jpg" class="img-responsive" alt="">
                            </div>
                            </a>
                            <div class="title-feature">
                                <a href="" title="">
                                <h3>Estudos e artigos</h3>
                                <p>Comparison between Platelet-Rich Plasma and Autologous Iliac Grafts for Tibial Osteotomy, Cartilage, 2010</p>
                                </a>
                            </div>
                        </li>
                        <li>
                            <a href="" title="">
                            <div class="thumb-feature">
                                <img src="<?php echo get_template_directory_uri(); ?>/static/images/ex-video.jpg" class="img-responsive" alt="">
                            </div>
                            </a>
                            <div class="title-feature">
                                <a href="" title="">
                                <h3>Judô: dicas para a prática desse esporte</h3>
                                <p>Saiba mais sobre essa modalidade, seus benefícios e cuidados necessários</p>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
