<?php
/**
 * @package WordPress
 * @subpackage Instituto Vita
 * @since Instituto Vita 1.0
 */
 get_header(); ?>

	<h2><?php _e('Error 404 - Page Not Found','institutovita'); ?></h2>

<?php get_sidebar(); ?>

<?php get_footer(); ?>