<?php
/**
 * @package WordPress
 * @subpackage Instituto Vita
 * @since Instituto Vita 1.0
 */
?><!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head id="<?php echo of_get_option('meta_headid'); ?>" data-template-set="institutovita-wordpress-theme">

	<meta charset="<?php bloginfo('charset'); ?>">

	<!-- Always force latest IE rendering engine (even in intranet) -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->

	<?php
		if (is_search())
			echo '<meta name="robots" content="noindex, nofollow" />';
	?>

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">

	<!--Google will often use this as its description of your page/site. Make it good.-->
	<meta name="description" content="<?php bloginfo('description'); ?>" />

	<?php
		if (true == of_get_option('meta_author'))
			echo '<meta name="author" content="' . of_get_option("meta_author") . '" />';

		if (true == of_get_option('meta_google'))
			echo '<meta name="google-site-verification" content="' . of_get_option("meta_google") . '" />';
	?>

	<meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">

	<?php
		/*
			j.mp/mobileviewport & davidbcalhoun.com/2010/viewport-metatag
			 - device-width : Occupy full width of the screen in its current orientation
			 - initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
			 - maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width
		*/
		if (true == of_get_option('meta_viewport'))
			echo '<meta name="viewport" content="' . of_get_option("meta_viewport") . '" />';


		/*
			These are for traditional favicons and Android home screens.
			 - sizes: 1024x1024
			 - transparency is OK
			 - see wikipedia for info on browser support: http://mky.be/favicon/
			 - See Google Developer docs for Android options. https://developers.google.com/chrome/mobile/docs/installtohomescreen
		*/
		if (true == of_get_option('head_favicon')) {
			echo '<meta name=”mobile-web-app-capable” content=”yes”>';
			echo '<link rel="shortcut icon" sizes=”1024x1024” href="' . of_get_option("head_favicon") . '" />';
		}


		/*
			The is the icon for iOS Web Clip.
			 - size: 57x57 for older iPhones, 72x72 for iPads, 114x114 for iPhone4 retina display (IMHO, just go ahead and use the biggest one)
			 - To prevent iOS from applying its styles to the icon name it thusly: apple-touch-icon-precomposed.png
			 - Transparency is not recommended (iOS will put a black BG behind the icon) -->';
		*/
		if (true == of_get_option('head_apple_touch_icon'))
			echo '<link rel="apple-touch-icon" href="' . of_get_option("head_apple_touch_icon") . '">';
	?>

	<!-- Application-specific meta tags -->
	<?php
		// Windows 8
		if (true == of_get_option('meta_app_win_name')) {
			echo '<meta name="application-name" content="' . of_get_option("meta_app_win_name") . '" /> ';
			echo '<meta name="msapplication-TileColor" content="' . of_get_option("meta_app_win_color") . '" /> ';
			echo '<meta name="msapplication-TileImage" content="' . of_get_option("meta_app_win_image") . '" />';
		}

		// Twitter
		if (true == of_get_option('meta_app_twt_card')) {
			echo '<meta name="twitter:card" content="' . of_get_option("meta_app_twt_card") . '" />';
			echo '<meta name="twitter:site" content="' . of_get_option("meta_app_twt_site") . '" />';
			echo '<meta name="twitter:title" content="' . of_get_option("meta_app_twt_title") . '">';
			echo '<meta name="twitter:description" content="' . of_get_option("meta_app_twt_description") . '" />';
			echo '<meta name="twitter:url" content="' . of_get_option("meta_app_twt_url") . '" />';
		}

		// Facebook
		if (true == of_get_option('meta_app_fb_title')) {
			echo '<meta property="og:title" content="' . of_get_option("meta_app_fb_title") . '" />';
			echo '<meta property="og:description" content="' . of_get_option("meta_app_fb_description") . '" />';
			echo '<meta property="og:url" content="' . of_get_option("meta_app_fb_url") . '" />';
			echo '<meta property="og:image" content="' . of_get_option("meta_app_fb_image") . '" />';
		}
	?>

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

	<!-- build:js(.) static/js/modernizr.js -->
	<script src="<?php echo get_template_directory_uri(); ?>/<?php echo get_template_directory_uri(); ?>/dev/bower_components/modernizr/modernizr.js"></script>
    <!-- endbuild -->

	<!-- Custom CSS add here -->

	<!-- build:css(.) static/css/vendor.css -->
	<!-- bower:css -->
	<link rel='stylesheet' href='dev/bower_components/bootstrap/dist/css/bootstrap.css' />
	<!-- endbower -->
	<!-- endbuild -->

	<!-- build:css(.tmp) static/css/main.css -->
	<link rel='stylesheet' href='styles/ie.css' />
	<link rel='stylesheet' href='styles/style.css' />
    <!-- endbuild -->

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<div id="fb-root"></div>
    <script>
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=458991677600594";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

	<header class="topbar">
        <div class="container">
            <nav id="navbar-top" class="container navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/static/images/logo_Vita.png" height="40" width="101" alt="Instituto Vita">
                        </a>
                    </div>
                    <div id="navbar-access">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Meu Vita <span class="caret"></span></a>
                                <ul class="dropdown-menu drop-form" role="menu">
                                    <form>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">CPF:</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="CPF:">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Senha:</label>
                                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Senha">
                                        </div>
                                        <button type="submit" class="btn btn-default">Entrar</button>
                                    </form>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="glyphicon glyphicon-search"></i></span></a>
                                <ul class="dropdown-menu drop-search" role="menu">
                                    <form>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Procurar por...">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button">Buscar</button>
                                            </span>
                                        </div>
                                    </form>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>

    <div class="main-menu">
        <div class="container">
            <nav id="navbar-vita" class="container navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Home</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Conheça o Vita <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Quem somos</a></li>
                                    <li class="menu-item dropdown dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Especialidades</a>
                                        <ul class="dropdown-menu">
                                            <li class="menu-item dropdown dropdown-submenu">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Ortopedia e Traumatologia</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Coluna</a></li>
                                                    <li><a href="#">Cotovelo</a></li>
                                                    <li><a href="#">Joelho</a></li>
                                                    <li><a href="#">Mão</a></li>
                                                    <li><a href="#">Cirurgia Buco Maxilo Facial</a></li>
                                                    <li><a href="#">Ombro</a></li>
                                                    <li><a href="#">Oncologia Ortopédica</a></li>
                                                    <li><a href="#">Pé e Tornozelo</a></li>
                                                    <li><a href="#">Quadril</a></li>
                                                    <li><a href="#">Trauma Ortopédico</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item dropdown dropdown-submenu">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reabilitação</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Fisioterapia</a></li>
                                                    <li><a href="#">Hidroterapia</a></li>
                                                    <li><a href="#">Osteopatia Postural</a></li>
                                                    <li><a href="#">Preparação Física</a></li>
                                                    <li><a href="#">Terapia Ocupacional</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Laboratório de Biomecânica</a></li>
                                            <li class="menu-item dropdown dropdown-submenu">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Vitalidade</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Cardiologia</a></li>
                                                    <li><a href="#">Clínica Médica</a></li>
                                                    <li><a href="#">Medicina Esportiva</a></li>
                                                    <li><a href="#">Nutrição</a></li>
                                                    <li><a href="#">Psicologia</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item dropdown dropdown-submenu">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Odontologia</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Cirurgia Bucal Maxilo Facial</a></li>
                                                    <li><a href="#">Clínica Geral</a></li>
                                                    <li><a href="#">Implante</a></li>
                                                    <li><a href="#">Ortodontia</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Unidades</a></li>
                                    <li><a href="#">Estrutura</a></li>
                                    <li><a href="#">Profissionais</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Histórias de Vita</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Vita Care <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Conheça Vita Care</a></li>
                                    <li><a href="#">Quem se qualifica</a></li>
                                    <li><a href="#">Nossos atletas</a></li>
                                    <li><a href="#">Pré-consulta</a></li>
                                    <li><a href="#">Parceiros</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Ensino e pesquisa <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Cursos</a></li>
                                    <li><a href="#">Parceiros</a></li>
                                    <li><a href="#">Pesquisa</a></li>
                                    <li><a href="#">Artigos</a></li>
                                    <li><a href="#">Treinamento profissional</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Saiba Mais <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Notícias</a></li>
                                    <li><a href="#">Vídeos</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Faça parte do Vita</a></li>
                            <li><a href="#">Contato</a></li>
                            <li class="dropdown hide-screen">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Meu Vita <span class="caret"></span></a>
                                <ul class="dropdown-menu drop-form" role="menu">
                                    <form>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">CPF:</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="CPF:">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Senha:</label>
                                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Senha">
                                        </div>
                                        <button type="submit" class="btn btn-default">Entrar</button>
                                    </form>
                                </ul>
                            </li>
                            <li class="dropdown hide-screen">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="glyphicon glyphicon-search"></i></span></a>
                                <ul class="dropdown-menu drop-search" role="menu">
                                    <form>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Procurar por...">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button">Buscar</button>
                                            </span>
                                        </div>
                                    </form>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>

	<!-- <div id="wrapper" class="container"> -->

		<!-- <header id="header" role="banner">
			<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<div class="description"><?php bloginfo( 'description' ); ?></div>
		</header>

		<nav id="nav" role="navigation">
			<?php wp_nav_menu( array('menu' => 'primary') ); ?>
		</nav> -->

