<?php
/**
 * @package WordPress
 * @subpackage Instituto Vita
 * @since Instituto Vita 1.0
 */
?>

    <!-- </div> -->

    <!-- <footer id="footer" class="source-org vcard copyright" role="contentinfo">
    	<small>&copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?></small>
    </footer> -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 mgt-30">
                    <div class="text-center">
                        <span class="ft1">+ SOMENTE URGÊNCIA</span>
                        <span class="ft2">(11) 95770-3600</span>
                        <p class="txt-urgencia">O Grupo de Atendimento de Trauma do Instituto Vita está capacitado a assistir todos os tipos de Trauma e Urgências Ortopédicas nos melhores hospitais de São Paulo.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 mgt-30">
                    <h1 class="text-center">Nossas unidades</h1>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner text-center" role="listbox">
                            <div class="item active">
                                <p>Unidade Higienópolis</p>
                                <p> Rua Mato Grosso, 306 - 1º andar Higienópolis - São Paulo / SP Segunda a Sexta das 6:00 às 20:00 Sábados (plantão) das 7:30 às 12:30</p>
                                <div class="carousel-caption"></div>
                            </div>
                            <div class="item">
                                <p>Lorem ipsum.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam sapiente optio neque quidem libero itaque!</p>
                                <div class="carousel-caption"></div>
                            </div>
                        </div>
                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 mgt-30 text-center">
                    <h1>Estamos conectados</h1>
                    <div class="fb-page" data-href="https://www.facebook.com/InstitutoVita1" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/InstitutoVita1"><a href="https://www.facebook.com/InstitutoVita1">Instituto Vita</a></blockquote></div></div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-left">
                            <p style="margin-bottom: 0;">Copyright ©<script type="text/javascript">var theDate = new Date(); document.write(theDate.getFullYear());</script>. Instituto Vita. Todos os direitos reservados</p>
                        </div>
                        <div class="pull-right">
                            <div class="social-links">
                                <a href="https://www.facebook.com/InstitutoVita1" target="_blank" title="">
                                    <img src="<?php echo get_template_directory_uri(); ?>/static/images/social-face.svg" height="32" width="32" alt="">
                                </a>
                                <a href="https://www.linkedin.com/company/instituto-vita" target="_blank" title="">
                                    <img src="<?php echo get_template_directory_uri(); ?>/static/images/social-linkedin.svg" height="32" width="32" alt="">
                                </a>
                                <a href="https://www.youtube.com/user/institutovita" target="_blank" title="">
                                    <img src="<?php echo get_template_directory_uri(); ?>/static/images/social-youtube.svg" height="32" width="32" alt="">
                                </a>
                                <a href="" target="_blank" title="">
                                    <img src="<?php echo get_template_directory_uri(); ?>/static/images/social-instagram.svg" height="32" width="32" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <?php wp_footer(); ?>

    <!-- build:js(.) static/js/vendor.js -->
    <!-- bower:js -->
    <script src="dev/bower_components/jquery/jquery.js"></script>
    <script src="dev/bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <!-- endbower -->
    <!-- endbuild -->
    <!-- build:js(.) static/js/main.js -->
    <script src="dev/src/js/header.js"></script>
    <script src="dev/src/js/footer.js"></script>
    <!-- endbuild -->

    <!-- Asynchronous google analytics; this is the official snippet. Replace UA-XXXXXX-XX with your site's ID and domainname.com with your domain, then uncomment to enable.
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-XXXXXX-XX', 'domainname.com');
      ga('send', 'pageview');

    </script>
    -->

    </body>

</html>