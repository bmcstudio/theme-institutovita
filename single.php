<?php
/**
 * @package WordPress
 * @subpackage Instituto Vita
 * @since Instituto Vita 1.0
 */
 get_header(); ?>

	<div class="container" style="margin-top: 60px;">
		<div id="main">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

					<div class="post-header">
						<span class="cat"><a href="" rel="category tag"><?php the_category(', ') ?></a></span>
							<h1><?php the_title(); ?></h1>
						<span class="title-divider"></span>
						<span class="post-date"><?php posted_on(); ?></span>
					</div>
					<div class="post-img">
						<img width="1080" height="715" src="http://solopine.com/redwood/wp-content/uploads/2015/06/post8-1080x715.jpg" class="attachment-full-thumb wp-post-image" alt="">
					</div>

					<div class="entry-content">

						<?php the_content(); ?>

						<?php wp_link_pages(array('before' => __('Pages: ','institutovita'), 'next_or_number' => 'number')); ?>

						<?php the_tags( __('Tags: ','institutovita'), ', ', ''); ?>

					</div>

					<?php edit_post_link(__('Edit this entry','institutovita'),'','.'); ?>

				</article>

			<?php comments_template(); ?>

			<?php endwhile; endif; ?>
			<?php post_navigation(); ?>

		</div>

		<?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>