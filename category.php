<ul class="sp-grid">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <li>
    		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

                <div class="post-img">
                    <a href="<?php the_permalink() ?>"><img width="520" height="400" src="http://solopine.com/redwood/wp-content/uploads/2015/06/post14-520x400.jpg" class="attachment-misc-thumb wp-post-image" alt="post14"></a>
                </div>
                <div class="post-header">
                    <span class="cat"><a href="" rel="category tag"><?php the_category(', ') ?></a></span>
                    <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                    <span class="title-divider"></span>
                </div>
                <div class="post-entry">
                    <?php the_content(); ?>
                </div>
                <div class="list-meta">
                    <span class="post-date"><?php posted_on(); ?></span>
                </div>
    		</article>
        </li>

	<?php endwhile; ?>
</ul>

<?php post_navigation(); ?>

<?php else : ?>

	<h2><?php _e('Nothing Found','institutovita'); ?></h2>

<?php endif; ?>
